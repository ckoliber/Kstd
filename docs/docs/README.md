# kstd (KoLiBer Standard Library)

## Owner

* [KoLiBer](https://koliber.ir)

## Author

* [KoLiBer](https://koliber.ir)

## Developers

1. [KoLiBer](https://koliber.ir)

## Start Date (UTC)

* __Wed, 08 Aug 2018 11:13:41 GMT__

## Question

* How can we code a cross platform application in standard and simple way in high performance C ?

## Goal

* kstd is a project for standardizing C language in `DSA`, `Network`, `File`, `Security`, etc for __cross platform__ usage (`android`, `ios`, `winapi32`, `linux`)

## License

* Project is under __[MIT](LICENSE.md)__ license

![MIT License](https://pre00.deviantart.net/4938/th/pre/f/2016/070/3/b/mit_license_logo_by_excaliburzero-d9ur2lg.png)

## Languages and Frameworks

1. __Client__
    1. __C-11__

## SDLC (Software Development Life Cycle)

* Project is under __`agile`__ SDLC

## Links

1. [Zoho Sprints](https://sprints.zoho.com/team/koliber#plan/P2)
2. [Gitlab](https://gitlab.com/ckoliber/kstd)